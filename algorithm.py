import cv2
import numpy as np
import matplotlib.pyplot as plt
import random

# Lese Pixel aus Bild als Daten ein
def dataFromImage(path):
    dataImage = cv2.imread(path)
    dataImage = cv2.cvtColor(dataImage, cv2.COLOR_BGR2GRAY)
    xMax = dataImage.shape[1]
    yMax = dataImage.shape[0]

    data = []
    k = 4

    for y in range(yMax):
        for x in range(xMax):
            if dataImage[y, x] == 0:
                data.append([x, yMax - y])

    data = np.asarray(data, dtype="float64")

    # Normalisiere die Daten
    data[:, 0] = data[:, 0] / data[:, 0].max()
    data[:, 1] = data[:, 1] / data[:, 1].max()

    return data

# KMeans Algorithmus
class KMeans:
    def __init__(self, data, k):
        self.data = data
        self.k = k
    
    # Zufällige Startzentren
    def initializeCentroids(self):
        M = []
        for i in range(self.k):
            x = random.random()
            y = random.random()
            for j in range(len(M)):
                while M[j][0] == x and M[j][1] == y:
                    x = random.random()
                    y = random.random()
            m = [x, y]
            M.append(m)
        return np.asfarray(M)

    # Euklidischer Abstand zum Quadrat
    def dSqaure(point1, point2):
        return (point1[0] - point2[0])**2 + (point1[1] - point2[1])**2

    # Eigentliche Algorithmus
    def llyod(self, M = [], draw=True):
        # Falls keine Startzentren übergeben werden
        if len(M) == 0:
            M = self.initializeCentroids()

        if draw:
            # Plot konfigurieren
            plt.rcParams['mathtext.fontset'] = 'cm'
            plt.rcParams['font.family'] = 'STIXGeneral'
            fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)
            axs[0, 0].set_title("(1)")
            axs[0, 1].set_title("(2)")
            axs[1, 0].set_title("(3)")
            axs[1, 1].set_title("(4)")
            axs[0, 0].scatter(self.data[:, 0], self.data[:, 1], color="black", s=2)
            axs[0, 0].scatter(M[:, 0], M[:, 1], color="purple")
        
        # Damit die ersten drei Schritte und das finale Clustering geplottet werden kann
        step = 0

        convergence = False

        while not convergence:
            S = [[] for i in range(self.k)]
            for x in self.data:
                # Berechne den Abstand zu den Clusterzentren
                ds = [KMeans.dSqaure(x, m) for m in M]
                if len(ds) > 0:
                    s = np.argmin(ds)
                    # Füge x dem nächst gelegenen Cluster hinzu
                    S[s].append(x)
            
            changedCentroid = False
            for i, s in enumerate(S):
                if len(s) > 0:
                    # Berechne die Clusterzentren neu
                    mean = np.mean(s, axis=0)
                    if not np.array_equal(mean, M[i]):
                        M[i] = mean
                        changedCentroid = True

            # S in ein Numpy Array konvertieren    
            S = np.array([np.array(s) for s in S], dtype=object)
            if draw:
                step += 1
                if step == 1:
                    KMeans.drawClusters(S, M, axs[0, 1])
                elif step == 2:
                    KMeans.drawClusters(S, M, axs[1, 0])
            
            convergence = not changedCentroid
        
        if draw:
            # Finales Clustering plotten
            KMeans.drawClusters(S, M, axs[1, 1])
            plt.show()
        return S, M
        
    def drawClusters(S, M, axis):
        # Draw data points
        colors = ["red", "blue", "green", "purple", "orange", "yellow", "gray", "brown", "ocean"]
        for i, s in enumerate(S):
            if len(s) > 0:
                c = colors[i]
                axis.scatter(S[i][:, 0], S[i][:, 1], color=c, alpha=1, s=2)
                # Draw Centroid
                axis.scatter(M[i, 0], M[i, 1], color=c, marker="+", s=100, linewidth=3)

# Klasse um den KMeans Algorithmus zu validieren
class Validity:
    def __init__(self, data):
        self.data = data
        self.min = 2
        self.max = 8
        self.N = len(data)
        print("Validiere ", self.N, " Datenpunkte")
    
    # Berechnet den Davies-Bouldin Index
    def index(self, k):
        kMeans = KMeans(self.data, k)
        S, M = kMeans.llyod(draw=False)
        if k > 1:
            return self.intra(S, M) / self.inter(M), S, M
        else:
            return self.intra(S, M), S, M
    
    # Berechnet die sum of sqaured errors
    def sse(self, k):
        kMeans = KMeans(self.data, k)
        S, M = kMeans.llyod(draw=False)
        sum = 0
        for i, m in enumerate(M):
            xi = S[i]
            for x in xi:
                distance = KMeans.dSqaure(x, m)
                sum = sum + distance

        return sum, S, M

    def intra(self, S, M):
        sum = 0
        for i, m in enumerate(M):
            for x in S[i]:
                sum += KMeans.dSqaure(x, m)

        return sum / self.N

    def inter(self, M):
        min = 10 # There always is a value smaller than 10
        for i in range(len(M) - 1):
            for j in range(i + 1, len(M)):
                d = KMeans.dSqaure(M[i], M[j])
                if d < min:
                    min = d
        return min
    
    def validate(self, method = "sse"):
            
        result = []
        diagrams = []
        for i in range(self.min, self.max):
            if method == "sse":
                index, S, M = self.sse(i)
            else:
                index, S, M = self.index(i)
            result.append([i, index])
            diagrams.append([S, M])

        result = np.array(result)

        minIndex = np.argmin(result[:, 1])
        
        if method == "sse":
            # Hier müsste noch der richtige Index bestimmt werden
            minIndex = 1
        print("K: ", result[minIndex, 0])
        S, M = diagrams[minIndex]
        # Plot konfigurieren
        plt.rcParams['mathtext.fontset'] = 'cm'
        plt.rcParams['font.family'] = 'STIXGeneral'
        fig, axs = plt.subplots(1, 2)
        axs[0].set_box_aspect(1)
        axs[1].set_box_aspect(1)
        axs[0].set_title("(a)")
        axs[1].set_title("(b)")
        axs[0].set_ylabel(method)
        axs[0].set_xlabel("k")
        axs[0].plot(result[:, 0], result[:, 1], color="black", linewidth=1)

        KMeans.drawClusters(S, M, axs[1])

        plt.show()

# Grafik für falsche Initialisierung der Cluster Zentren
def createGraphic():
    alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
    data = [[0.1, 0.9], [0.3, 0.7], [0.3, 0.8], [0.8, 0.4], [0.9, 0.4], [0.2, 0.1], [0.3, 0.1]]
    data = np.array(data)

    # Setup plot
    plt.rcParams['mathtext.fontset'] = 'cm'
    plt.rcParams['font.family'] = 'STIXGeneral'
    plt.xlim(0, 1)
    plt.ylim(0, 1)        
    fig, a = plt.subplots(1, 2, sharex=True, sharey=True)
    a[0].set_title("(a)")
    a[0].set_aspect("equal", "box")
    a[1].set_title("(b)")
    a[1].set_aspect("equal", "box")
    for i, x in enumerate(data):
        a[0].text(x[0], x[1], alphabets[i], size=15)

    kMeans = KMeans(data, 3)
    kMeans.llyod(M = np.array([data[0], data[1], data[2]]))


if __name__ == "__main__":
    data = dataFromImage("data2.png")
    k = KMeans(data, 2)
    k.llyod()

    v = Validity(data)
    v.validate(method="validity")



